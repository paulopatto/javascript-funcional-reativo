/**
 * Escreva duas funções que:
 * 
 * 1. Consiga fazer as seguintes execuções: somar(2)(3)(5) & somar(2)(4)(10)...
 * 2. Consiga executar calcular(int)(int)(fn: op) exemplo: calcular(3)(5)(somar)
 */
const chai    = require('chai');
const expect  = chai.expect;
const {somar, potencia, calcular, dividir, multiplicar, subtrair}  = require('../src/aula16');

describe("#somar: Uma função somar que vai somando conforme vai recendo parametros", () => {
    describe("Case 2 parametros", () => {
        it('deve retornar a soma dos dois', () => {
            expected_value = 6 + 4;
            returned_value = +somar(6)(4);

            expect(returned_value).to.equal(expected_value);
        });
    });

    describe("Case 3 parametros", () => {
        it('deve retornar a soma dos três', () => {
            expected_value = 6 + 4 + 10;
            returned_value = +somar(6)(4)(10);
            expect(returned_value).to.equal(expected_value);
        });
    });

    describe("Case 4 parametros", () => {
        it('deve retornar a soma dos quatro', () => {
            expected_value = 6 + 4 + 10 + 80;
            returned_value = +somar(6)(4)(10)(80);
            expect(returned_value).to.equal(expected_value);
        });
    });
});


describe("#calcular: Consiga executar calcular(int)(int)(fn: op) exemplo: calcular(3)(5)(somar)", () => {
    it("should calculate sum with somar", () => {
        expected_value = 4 + 3;
        returned_value = calcular(4)(3)(somar)
    });

    
    it("should calculate minus with subtrair", () => {
        expected_value = 4 - 3;
        returned_value = calcular(4)(3)(subtrair)
    });


    it("should calculate multiply with multiplicar", () => {
        expected_value = 4 * 3;
        returned_value = calcular(4)(3)(multiplicar)
    });

    it("should calculate div with dividir", () => {
        expected_value = 16 / 4;
        returned_value = calcular(16)(4)(dividir)
    });
});

describe("#potencia", () => {
    it('should return correct value', () => {
        expected_value = 9
        returned_value = potencia(3)(2)
        expect(returned_value).to.equal(expected_value);
    });
});
