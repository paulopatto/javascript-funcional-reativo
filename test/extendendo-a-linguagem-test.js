const chai    = require('chai');
const expect  = chai.expect;

Array.prototype.last = function() {
    return this[this.length - 1];
};

Array.prototype.first = function () { return this[0]; };

describe("Array", () => {
    let subject = [1, 2, 3, 4, 5, 6, 7, 8, 9];

    describe(".last", () => {
        it("should return the last element of array", () => {
            expected_value = 9;

            expect(subject.last()).to.equal(expected_value);
        });
    });

    describe(".first", () => {
        it("should return the first (head) element of array", () => {
            expected_value = 1;

            expect(subject.first()).to.equal(expected_value);
        });

    });
});
