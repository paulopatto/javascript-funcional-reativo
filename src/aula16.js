/**
 * Aula 16: Basico sobre funções
 */
exports.somar = (value) => {
    var sum = value;

    function somador(n) { 
        sum += n;
        return somador;
    };

    somador.valueOf = () => { return sum; };

    return somador;
};

exports.potencia = (base) => {
    return (expoente) => { return Math.pow(base, expoente); }
};

exports.calcular = function(a) {
    return function(b) {
        return function(op) {
            return op(a)(b);
        }
    }
}

exports.subtrair = function (value) { 
    var calc = value;

    function subtrator(n) {
        calc -= n;
        return subtrator;
    };

    subtrator.valueOf = () => { return calc; };

    return subtrator;
};

exports.multiplicar = (value) => { 
    var calc = value;

    function multiplicador(n) {
        calc *= n;
        return multiplicador;
    };

    multiplicador.valueOf = () => { return calc; };

    return multiplicador;
};

exports.dividir = (value) => { 
    var calc = value;

    function divisor(n) {
        calc =  calc / n;
        return divisor;
    };

    divisor.valueOf = () => { return calc; };

    return divisor;
};
