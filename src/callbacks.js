const fs = require('fs');
const path = require('path');

const caminho = path.join(__dirname, 'lorem-ipsum.txt');

function exibirConteudo(err, conteudo){
    console.log(conteudo.toString());
}

console.log("--- Inicio");
fs.readFile(caminho, {}, exibirConteudo);
fs.readFile(caminho, (_, conteudo) => console.log(conteudo.toString()));
console.log("--- Fim: Veja como a leitura foi async"); 

console.log("--- INICIO SYNC");
console.log(fs.readFileSync(caminho).toString());
console.log("--- FIM SYNC ---");

